<?php
session_start();
require 'mysql.php';
function handle_new_post() {
    $title = $_POST['title'];
    $text = $_POST['text'];
    if (isset($title) and isset($text)) {
        if (new_post($title, $text)) echo "New Post has been added";
        else echo "No post is added";
    }
}
handle_new_post();
?>
<html><h1> Write a post </h1></html>
<form action="newpost.php" method="POST" class="form post">
	<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>"/>
	Name:<input type="text" name="user" /> <br>
	Title:<input type="text" name="title" /> <br>
	Text: <textarea name="text" required cols="100" rows="10"></textarea><br>
	<button class="button" type="submit">
		Create post
	</button>
</form>
