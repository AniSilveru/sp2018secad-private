/* include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h> 
#include <netdb.h>
#include <string.h>


int main (int argc, char *argv[])
{

   	char hostname[1000], path[1000];
	char d[1000];

	strncpy(d,argv[1],1000);
	sscanf(d,"http://%[^/]/%s",hostname,path);

	printf("\n\nApplication development by Anitha Silveru for Assignment1.\n");
	printf("\npath is %s\n",path);

	//int connect(int sockfd, struct sockaddr *serveraddr, int addrlen);
	int port=80;
		

	
	if(argc!=2){
	printf("Usage: %s <url>\n", argv[1]);
	exit(0);
}
/*if(argc!=3){
	printf("Usage: %s <servername> <port>", argv[0]);
	exit(0);
}

	char *servername= argv[1];
	int port = atoi(argv[2]);
	printf("Servername: %s Port: %d\n",servername, port); */

	
	//Socket	

	printf("\nHost = %s, Port = %d\n", hostname,port);
	
	int sockfd = socket (AF_INET, SOCK_STREAM ,0);
	if(sockfd<0)
	{ 
	perror("cannot create socket");
	exit(1);
}

	printf("A socket is opened\n");
	struct hostent *server_he;
	if((server_he = gethostbyname(hostname)) == NULL){
	perror("Cannot resolve the hostname");
	exit(2);
}

	//Server connection
	struct sockaddr_in serveraddr;
	
	bzero((char *) &serveraddr, sizeof (serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *) server_he->h_addr, 
		(char *) &serveraddr.sin_addr.s_addr,
		server_he->h_length);
	
	serveraddr.sin_port= htons(port); //setting port number

	int connected= connect(sockfd,(struct sockaddr *) &serveraddr, sizeof(serveraddr));

	//connect: create a connection to the server
	if(connected < 0){
	perror("Cannot connect to server");
	exit(3);
	}else 
	printf("connected to the server at  port %d\n",port); 

	//HTTP request
	char request[255];
	sprintf(request,"GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n",path,hostname);
	
	char str[80];
	char *host=path;
	sprintf(str,"Host: %s\r\n\r\n", host);
	
	// char *msg = "This is just a test message from client";
	int byte_sent = send(sockfd,request, strlen(request), 0); //sending the data
	
	int byte_received; //for receiving the data
	char buffer[10000];
	//printf("Enter your message:");
	bzero(buffer,10000);
	byte_received = recv(sockfd,buffer,10000,4096);
	if(byte_received<0){
	error("Error in reading.");
	}
	
	//calculating index
	char *ad;
	int index;
	ad = strchr(buffer, 'O');
	index = (int)(ad - buffer);

	char *ret;
	ret = strstr(buffer,"\r\n\r\n");

	if(index == 15)
	{
        //data part 
        printf("\nThe data part of the message :\n%s\n", ret+4);

        //filename
        const char ch = '/';
        char *rec; 
        rec = strrchr(path, ch); 

        if(rec!= NULL)
        {
        FILE *fp;
        fp = fopen(rec+1, "w");
       
	if(fp == NULL)
        {
        printf("Error in oppening the file 2");
        exit(1);
        }
          
        int bytes_remaining = byte_received-((ret-buffer)+4);
        fwrite(ret+4, bytes_remaining,1,fp);
        while((byte_received=recv(sockfd,buffer,10000,0))!=0) {

         fwrite(buffer,byte_received,1,fp);   
        }
        fclose(fp);
        }

        else{
        //writing the content
        FILE *fileptr;
        fileptr = fopen(path, "w");

        if(fileptr == NULL)
        {
              printf("\nError in opening the file\n");
              exit(1);
        }    
        int bytes_remaining = byte_received-((ret-buffer)+4);
        fwrite(ret+4, bytes_remaining,1,fileptr);
        while((byte_received=recv(sockfd,buffer,10000,0))!=0) {

        fwrite(buffer,byte_received,1,fileptr);
	
        }
        fclose(fileptr);
        }
}

	else{

	//printing the buffer for status is not 200 ok
	printf("\nThe message recieved is: %s", buffer);
	printf("\nThe content is not copied into the file as the status is not 200 OK \n\n");

	char *file1;
	int index1;
	file1 = strchr(buffer, '\n');
	index1 = (int)(file1 - buffer);

	char *msg_status=(char *) malloc(100);
	strncpy(msg_status, buffer+9,index1);
	printf("Status message is : %s\n\n",msg_status);

}
	//printf("Received from server: %s",buffer);
	//close(sockfd);
	return 0;
}

