import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.lang.*;
import java.util.ArrayList;
import java.util.*;
import java.security.*;


public class EchoServer {

	public static ArrayList<Socket> socketList = new ArrayList<Socket>();
	public static  ArrayList<String> list=new ArrayList<String>();

	public static void main(String[] args) throws IOException {
	 int portNumber=8000;
	ServerSocket serverSocket = new ServerSocket(portNumber);
	System.out.println("Multi-threaded server started and running with the port " + serverSocket.getLocalPort()+"\n");
	while(true){
		Socket clientSocket = serverSocket.accept();
		EchoServerThread currentThread=new EchoServerThread(clientSocket);
		socketList.add(clientSocket);
		currentThread.start();
		String welcome_msg="\nWelcome to the chat session Please enter your username:password\n\n";
		clientSocket.getOutputStream().write(welcome_msg.getBytes("UTF-8"));
		}
	}
	}
class EchoServerThread extends Thread {
		public String username;
		public String partusr,partpswd;
		private Socket clientSocket= null;
		public EchoServerThread(Socket clientSocket){
		super("EchoServerThread");
		this.clientSocket = clientSocket;
		System.out.println("A client is connected from the  IP: " + clientSocket.getInetAddress().getHostAddress()+"\n");
	}
public void run(){
	try{
	BufferedReader in = new BufferedReader( new
	InputStreamReader(clientSocket.getInputStream()));
	String inputLine;
	boolean flag=true;
	while(true){
	inputLine=in.readLine();
	if(flag==true){
	       if(inputLine.equals("") || inputLine.trim().equals("") || inputLine.equals(null) || existsInList(inputLine).equals("present")){
		 String invalidUsernameMessage="You have entered an invalid username! Please enter a valid username \n\n";
	      	clientSocket.getOutputStream().write(invalidUsernameMessage.getBytes("UTF-8"));
	      	System.out.println("User has entered invalid username!! \n\n ");
	        flag=true;
	    }

	else{
		String[] parts=inputLine.split(":");
		partusr = parts[0];
		partpswd = parts[1];


	byte[] bytesOfMessage = partpswd.getBytes("UTF-8");

	MessageDigest md = MessageDigest.getInstance("MD5");
	byte[] thedigest = md.digest(bytesOfMessage);
	StringBuffer sb = new StringBuffer();
	for(int i=0; i<thedigest.length; i++){
		sb.append(Integer.toHexString(0xff & thedigest[i]));
	}

	      System.out.println("\nUsername of the user: " + partusr);
	      System.out.println("\npassword: " + sb.toString() + " has entered\n"); //message in server
	       String a="\n\nWelcome to the chat, "+partusr+"\n"+"Password: "+sb.toString()+"\nYour chat session has begun now.\nPlease enter the message you wish to send\nMore instructions below: \n"+"\"list\""+": to get the current list of users in this chat session\n"+"\"exit\"" +": to exit the chat session\n\n\n";//message to client
	      clientSocket.getOutputStream().write(a.getBytes("UTF-8"));
	      EchoServer.list.add(inputLine);
	      flag=false;

	    }
 }
	 else{
	   if(inputLine.equals("list")){
	    getListOfUsers();
	   }
	   else if(inputLine.equals("exit")){
	    exitMethod(username, clientSocket);
	   }
	   else{
	    System.out.println("received from user "+ partusr +" : " + inputLine+"\n");
	    sendToAll(username,inputLine,clientSocket);
		 }
	 }

}

}
catch (Exception e){

  exitMethod(username, clientSocket);
  }

}
public void getListOfUsers(){
	 String k="\nThe list of current users:\n";
	 try{
	  clientSocket.getOutputStream().write(k.getBytes("UTF-8"));
	 }
	 catch(Exception e){
	  System.out.println("Exception occured in printing the list of users");
	 }
	 for(int i=0;i<EchoServer.list.size();i++){
	  String m="\n"+EchoServer.list.get(i)+"\n";
	  try{
	  clientSocket.getOutputStream().write(m.getBytes("UTF-8"));
	  }
	  catch(Exception e){
	   System.out.println("Exception occured in printing the list of users");
	  }

	 }
}

public void exitMethod(String j, Socket clientSkt){


System.out.println("The user with username : "+ partusr +" has exited the chat");
	 try{

	  for(int k = 0; k < EchoServer.list.size(); k++){
		      String user = EchoServer.list.get(k);
		      if(user.equals(j)){
		          EchoServer.list.remove(k);
		      }
		        }
	  for(int l = 0; l < EchoServer.socketList.size(); l++){

	   Socket t = EchoServer.socketList.get(l);
		      if(t.equals(clientSkt)){
		          EchoServer.socketList.remove(l);
		      }
		 }
	  clientSkt.close();
	 }
	 catch(Exception e){
	  System.out.println("exited error");
	 }
	}

	public String existsInList(String c){
	 String k="not present";
	 for(int z = 0; z < EchoServer.list.size(); z++){
		    String user1 = EchoServer.list.get(z);
		    if(user1.equals(c)){
		        k="present";
	  break;
		    }
		}
	return k;

	}
void sendToAll(String userNameString ,String message, Socket clientSt){
	 ArrayList<Socket> tempList = new ArrayList<Socket>();
	 for(int j=0;j<EchoServer.socketList.size();j++){
		      tempList.add(EchoServer.socketList.get(j));
	 }

	 for(int l = 0; l < tempList.size(); l++){

		    if(tempList.get(l).equals(clientSt)){
		        tempList.remove(l);
		    }
		}

	 for(Socket skt : tempList){
	   String m1="\n"+partusr+" : "+message+"\n\n";
	   try{
	    skt.getOutputStream().write(m1.getBytes("UTF-8"));
	   }
	   catch(Exception e){
	    System.out.println("Exception occuerd in temporary socket listings");
	   }
	 }
	}
}
