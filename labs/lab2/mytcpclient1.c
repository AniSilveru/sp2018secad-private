/* include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h> 


int main (int argc, char *argv[])
{
   printf("Simple TCP client application development by Anitha Silveru for Lab 2.\n");

//int connect(int sockfd, struct sockaddr *serveraddr, int addrlen);

 
if(argc!=3){
	printf("Usage: %s <servername> <port>", argv[0]);
	exit(0);
}
	//printf("Server: %s Port: %s\n", argv[1], argv[2]);

	char *servername= argv[1];
	int port = atoi(argv[2]);
	printf("Servername: %s Port: %d\n",servername, port);
	
	int sockfd = socket (AF_INET, SOCK_STREAM ,0);
	if(sockfd<0)
	{ 
	perror("cannot create socket");
	exit(1);
}
	printf("A socket is opened\n");
	struct hostent *server_he;
	if((server_he = gethostbyname(servername)) == NULL){
	perror("Cannot resolve the hostname");
	exit(2);
}
	struct sockaddr_in serveraddr;
	
	bzero((char *) &serveraddr, sizeof (serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *) server_he->h_addr, 
		(char *) &severaddr.sin_addr.s_addr,
		server_he->h_length);
	
	serveraddr.sin_port= htons(port);

	int connected= connect(sockfd,(struct sockaddr *) &serveraddr, sizeof(serveraddr));

	//connect: create a connection to the server
	if(connected < 0){
	perror("Cannot connect to server");
	exit(3);
	}else 
	printf("connected to the server %s at  port %d\n",servername,port); 
	
	char *msg = "This is just a test message from client";
	int byte_sent;// = send(sockfd,msg, strlen(msg), 0);
	
	char buffer[1024];
	printf("Enter your message:");
	bzero(buffer,1024);
	fgets(buffer, 1024,stdin);
	byte_sent = send(sockfd, buffer, strlen(buffer),0);
	
	bzero(buffer,1024);
	int byte_received = recv(sockfd, buffer, 1024, 0);
	if(byte_received < 0){
	perror("Error in reading");
	exit(4);
	}
	printf("Received from server: %s",buffer);
	

	close(sockfd);
}

